package com.mastercard.footballtracker.db;


import android.support.annotation.NonNull;

import com.mastercard.footballtracker.models.PlayerEntity;

import java.util.List;

import io.requery.Nullable;

public interface FtDb {

    void addPlayer(@NonNull PlayerEntity entity);

    void updatePlayer(@NonNull PlayerEntity entity);

    void deletePlayer(int id);

    void deleteAllPlayers();

    @Nullable
    PlayerEntity getPlayer(String name);

    @NonNull
    List<PlayerEntity> getAllPlayers();


}

