package com.mastercard.footballtracker.db;

import android.content.Context;
import android.support.annotation.NonNull;

import com.mastercard.footballtracker.BuildConfig;
import com.mastercard.footballtracker.models.Models;
import com.mastercard.footballtracker.models.PlayerEntity;

import java.util.List;

import io.requery.EntityStore;
import io.requery.Nullable;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;

public class FtDbImpl implements FtDb {

    private static final int DATABASE_VERSION = 2;

    private static FtDb sInstance;
    private EntityDataStore<Persistable> mDataStore;

    private FtDbImpl(@NonNull EntityDataStore<Persistable> data) {
        mDataStore = data;
    }

    private static @NonNull
    EntityDataStore<Persistable> getDataStore(@NonNull Context context) {

        DatabaseSource source = new DatabaseSource(context, Models.DEFAULT, DATABASE_VERSION);

        if (BuildConfig.DEBUG) {
            source.setTableCreationMode(TableCreationMode.CREATE_NOT_EXISTS);
        }

        Configuration configuration = source.getConfiguration();
        return new EntityDataStore<Persistable>(configuration);
    }

    public static @NonNull FtDb getInstance(@NonNull Context context) {
        if (sInstance == null) {
            sInstance = new FtDbImpl(getDataStore(context));
        }

        return sInstance;
    }

    @Override
    public void addPlayer(@NonNull PlayerEntity entity) {
        mDataStore.upsert(entity);
    }

    @Override
    public void updatePlayer(@NonNull PlayerEntity entity) {
        mDataStore.update(entity);
    }

    @Override
    public void deletePlayer(int id) {
        mDataStore.delete(PlayerEntity.class)
                .where(PlayerEntity.ID.eq(id))
                .get();
    }

    @Override
    public void deleteAllPlayers() {
        mDataStore.delete(PlayerEntity.class)
                .get();
    }

    @Override
    public @Nullable
    PlayerEntity getPlayer(String name) {
        return mDataStore.select(PlayerEntity.class)
                .where(PlayerEntity.NAME.eq(name))
                .get()
                .firstOrNull();
    }

    @Override
    public @NonNull
    List<PlayerEntity> getAllPlayers() {
        return mDataStore.select(PlayerEntity.class)
                .get()
                .toList();
    }
}
