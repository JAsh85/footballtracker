package com.mastercard.footballtracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mastercard.footballtracker.db.FtDb;
import com.mastercard.footballtracker.db.FtDbImpl;
import com.mastercard.footballtracker.models.PlayerEntity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener {

    Button btnEmailData, btnClearData;
    RecyclerViewAdapter adapter;
    private FtDb mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mDatabase = FtDbImpl.getInstance(this);

        btnEmailData = findViewById(R.id.btn_end_session);
        btnClearData = findViewById(R.id.btn_clear_data);

        btnClearData.setOnClickListener((view) -> {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Clear Data?");
            alertDialog.setMessage("You will lose any unsent data.");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    (DialogInterface dialog, int which) -> {

                        for (PlayerEntity player : getAllPlayers()) {
                            player.setAttended(0);
                            player.setDrove(0);
                            player.setWins(0);
                            player.setGoals(0);
                            updatePlayer(player);
                        }

                        startActivity(new Intent(this, MainActivity.class));
                    });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                    (DialogInterface dialog, int which) -> dialog.dismiss());
            alertDialog.show();
        });

        btnEmailData.setOnClickListener((view) -> {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Email Data?");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    (DialogInterface dialog, int which) -> {

                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("message/rfc822");

                        // Load email from external source
                        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"jim@eagleeyetechnology.com"});

                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date date = new Date();
                        i.putExtra(Intent.EXTRA_SUBJECT, "EET 5-a-side Football " + dateFormat.format(date));

                        // Get the data
                        StringBuilder sb = new StringBuilder();

                        for (PlayerEntity player : getAllPlayers()) {
                            sb.append(player.getName() + ": " + player.getAttended() + ", " + player.getDrove() + ", " + player.getWins() + ", " + player.getGoals() + "\n");
                        }

                        i.putExtra(Intent.EXTRA_TEXT, sb.toString());

                        try {

                            startActivity(Intent.createChooser(i, "Send email..."));

                        } catch (android.content.ActivityNotFoundException ex) {

                            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                        }
                    });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                    (DialogInterface dialog, int which) -> dialog.dismiss());
            alertDialog.show();
        });

        // TODO: Fetch from external source
        ArrayList<String> playerNames = new ArrayList<>();
        playerNames.add("Ed");
        playerNames.add("Rob");
        playerNames.add("Jim");
        playerNames.add("Tom");
        playerNames.add("Jakub");
        playerNames.add("Scott");
        playerNames.add("Stephen");
        playerNames.add("Adam");
        playerNames.add("Shaun");
        playerNames.add("Jamie");


        for (String playerName : playerNames) {
            PlayerEntity player = getPlayer(playerName);
            if (player == null) {
                Log.d("jim", "Adding " + playerName + " to db.");
                addPlayer(playerName);
            }
        }

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rv_names);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerViewAdapter(this, playerNames);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position, PointType pointType) {

        String alertMessage = "";

        if (pointType == PointType.WIN) {

            alertMessage = adapter.getItem(position) + " - WIN";

        }

        if (pointType == PointType.GOAL) {

            alertMessage = adapter.getItem(position) + " - GOAL";

        }

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Confirm Action");
        alertDialog.setMessage(alertMessage);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                (DialogInterface dialog, int which) -> {

                    PlayerEntity playerEntity = getPlayer(adapter.getItem(position));

                    if (playerEntity != null) {

                        if (pointType == PointType.WIN) {

                            Toast.makeText(this, adapter.getItem(position) + " - Win", Toast.LENGTH_SHORT).show();
                            playerEntity.setWins(playerEntity.getWins() + 1);
                        }

                        if (pointType == PointType.GOAL) {

                            Toast.makeText(this, adapter.getItem(position) + " - Goal", Toast.LENGTH_SHORT).show();
                            playerEntity.setGoals(playerEntity.getGoals() + 1);
                        }

                        updatePlayer(playerEntity);

                    } else {

                        Toast.makeText(this, "playerEntity is null", Toast.LENGTH_SHORT).show();
                    }
        });


        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL", (DialogInterface dialog, int which) -> dialog.dismiss());
        alertDialog.show();
    }

    private void addPlayer(String name) {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setName(name);

        mDatabase.addPlayer(playerEntity);
    }

    private void updatePlayer(PlayerEntity player) {
        mDatabase.updatePlayer(player);
    }

    private void deletePlayer(int id) {
        mDatabase.deletePlayer(id);
    }

    private void deleteAllPlayers() {
        mDatabase.deleteAllPlayers();
    }

    private PlayerEntity getPlayer(String name) {
        return mDatabase.getPlayer(name);
    }

    private List<PlayerEntity> getAllPlayers() {
        return mDatabase.getAllPlayers();
    }

}
