package com.mastercard.footballtracker.models;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;


@Entity
public abstract class Player {

    @Key @Generated
    int id;

    String name;
    int attended;
    int drove;
    int wins;
    int goals;
}

