package com.mastercard.footballtracker;


public enum PointType {
    ATTENDED, DRIVE, GOAL, WIN
}
