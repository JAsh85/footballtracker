package com.mastercard.footballtracker;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class RecyclerViewAdapterScreen1 extends RecyclerView.Adapter<RecyclerViewAdapterScreen1.ViewHolder> {

    private List<String> mData = Collections.emptyList();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public RecyclerViewAdapterScreen1(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recylerview_row_screen_1, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String name = mData.get(position);
        holder.tvName.setText(name);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName;
        Button btnAttended;
        Button btnDrove;

        public ViewHolder(View itemView) {

            super(itemView);

            tvName = itemView.findViewById(R.id.tv_item_name);
            btnAttended = itemView.findViewById(R.id.btn_attended);
            btnDrove = itemView.findViewById(R.id.btn_drove);

            btnAttended.setOnClickListener(this);
            btnDrove.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            PointType pointType;

            switch (view.getId()) {

                case R.id.btn_attended:
                    pointType = PointType.ATTENDED;
                    break;
                default:
                    pointType = PointType.DRIVE;
                    break;
            }


            if (mClickListener != null)
                mClickListener.onItemClick(view, getAdapterPosition(), pointType);
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, PointType buttonType);
    }
}
