package com.mastercard.footballtracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mastercard.footballtracker.db.FtDb;
import com.mastercard.footballtracker.db.FtDbImpl;
import com.mastercard.footballtracker.models.PlayerEntity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AttendanceActivity extends AppCompatActivity implements RecyclerViewAdapterScreen1.ItemClickListener {

    Button btnProceed, btnClearData;
    RecyclerViewAdapterScreen1 adapter;
    private FtDb mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_attendance);

        mDatabase = FtDbImpl.getInstance(this);

        btnClearData = findViewById(R.id.btn_clear_data_screen_1);
        btnProceed = findViewById(R.id.btn_screen_1);

        btnClearData.setOnClickListener((view) -> {
            AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
            alertDialog.setTitle("Clear Data?");
            alertDialog.setMessage("You will lose any unsent data.");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    (DialogInterface dialog, int which) -> {

                        for (PlayerEntity player : getAllPlayers()) {
                            player.setAttended(0);
                            player.setDrove(0);
                            player.setWins(0);
                            player.setGoals(0);
                            updatePlayer(player);
                        }

                        startActivity(new Intent(this, AttendanceActivity.class));
                    });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                    (DialogInterface dialog, int which) -> dialog.dismiss());
            alertDialog.show();
        });

        btnProceed.setOnClickListener((view) -> {

            AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
            alertDialog.setTitle("Continue to next screen?");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    (DialogInterface dialog, int which) -> {

                        startActivity(new Intent(this, MainActivity.class));

                    });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                    (DialogInterface dialog, int which) -> dialog.dismiss());
            alertDialog.show();
        });

        // TODO: Fetch from external source
        ArrayList<String> allPlayerNames = new ArrayList<>();
        allPlayerNames.add("Ed");
        allPlayerNames.add("Rob");
        allPlayerNames.add("Jim");
        allPlayerNames.add("Tom");
        allPlayerNames.add("Jakub");
        allPlayerNames.add("Scott");
        allPlayerNames.add("Stephen");
        allPlayerNames.add("Adam");
        allPlayerNames.add("Shaun");
        allPlayerNames.add("Jamie");


        for (String playerName : allPlayerNames) {
            PlayerEntity player = getPlayer(playerName);
            if (player == null) {
                Log.d("jim", "Adding " + playerName + " to db.");
                addPlayer(playerName);
            }
        }

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rv_screen_1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerViewAdapterScreen1(this, allPlayerNames);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position, PointType pointType) {

        String alertMessage = "";

        if (pointType == PointType.ATTENDED) {

            alertMessage = adapter.getItem(position) + " - ATTENDED";

        }

        if (pointType == PointType.DRIVE) {

            alertMessage = adapter.getItem(position) + " - DRIVE";

        }

        AlertDialog alertDialog = new AlertDialog.Builder(AttendanceActivity.this).create();
        alertDialog.setTitle("Confirm Action");
        alertDialog.setMessage(alertMessage);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                (DialogInterface dialog, int which) -> {

                    PlayerEntity playerEntity = getPlayer(adapter.getItem(position));

                    if (playerEntity != null) {

                        if (pointType == PointType.ATTENDED) {

                            playerEntity.setAttended(1);
                        }

                        if (pointType == PointType.DRIVE) {

                            playerEntity.setDrove(1);
                        }

                        updatePlayer(playerEntity);

                    } else {

                        Toast.makeText(this, "playerEntity is null", Toast.LENGTH_SHORT).show();
                    }
        });


        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL", (DialogInterface dialog, int which) -> dialog.dismiss());
        alertDialog.show();
    }

    private void addPlayer(String name) {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.setName(name);

        mDatabase.addPlayer(playerEntity);
    }

    private void updatePlayer(PlayerEntity player) {
        mDatabase.updatePlayer(player);
    }

    private void deletePlayer(int id) {
        mDatabase.deletePlayer(id);
    }

    private void deleteAllPlayers() {
        mDatabase.deleteAllPlayers();
    }

    private PlayerEntity getPlayer(String name) {
        return mDatabase.getPlayer(name);
    }

    private List<PlayerEntity> getAllPlayers() {
        return mDatabase.getAllPlayers();
    }

}
